# ansible-git-status

A callback to check git status before playbook run.

## Requirements

Ansible >=2.6

The following python libraries are required:

- [`python-git`](https://github.com/chidimo/python-git)

## Installation

Once the required libraries (see above) have been installed:

1. Copy `git_status.py` to your playbook callback directory (by default
`callback_plugins/` in your playbook's root directory). Create the directory
if it doesn't exist.
2. add `git_status` to `callback_whitelist` in ansible.cfg.

## Configuraiton

See DOCUMENTATION variable for configuration options.
